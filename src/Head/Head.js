import React, { Component } from "react";

export default class Head extends Component {
  render() {
    return (
      <div
        style={{
          textAlign: "center",
          backgroundColor: "rgba(0, 0, 0, 0.7)",
          color: "white",
          padding: 30,
        }}
      >
        TRY GLASSES APP ONLINE
      </div>
    );
  }
}
