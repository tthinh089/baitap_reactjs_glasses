import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.css";
import { glassesArr } from "./data";

export default class Body extends Component {
  state = {
    imgURL: "",
  };

  handleChange = (glassesID) => {
    this.setState({
      imgURL: `../../glassesImage/v${glassesID}.png`,
    });
  };

  renderGlasses = () => {
    return glassesArr.map((glasses) => {
      return (
        <img
          key={glasses.id}
          onClick={() => {
            this.handleChange(glasses.id);
          }}
          className="col-2 p-3"
          style={{ cursor: "pointer", transition: "0.3s" }}
          src={glasses.url}
          alt="glasses img"
          onMouseOver={(e) => {
            e.target.style.transform = "scale(1.1)";
            e.target.style.opacity = 0.7;
          }}
          onMouseOut={(e) => {
            e.target.style.transform = "scale(1)";
            e.target.style.opacity = 1;
          }}
        />
      );
    });
  };

  render() {
    return (
      <div style={{ textAlign: "center", margin: 20 }}>
        <div
          style={{
            position: "relative",
            display: "inline-block",
            width: "20%",
          }}
        >
          <img
            src={"../../glassesImage/model.jpg"}
            width="100%"
            alt="model img"
          />
          <img
            src={this.state.imgURL}
            style={{
              position: "absolute",
              top: "33%",
              left: "50.5%",
              transform: "translate(-50%, -50%)",
              width: "50%",
              opacity: 0.9,
            }}
          />
        </div>
        <div
          className="row"
          style={{ background: "white", maxWidth: 1200, margin: "30px auto" }}
        >
          {this.renderGlasses()}
        </div>
      </div>
    );
  }
}
