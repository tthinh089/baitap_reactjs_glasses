import "./App.css";
import Body from "./Body/Body";
import Head from "./Head/Head";
import "bootstrap/dist/css/bootstrap.css";

function App() {
  return (
    <div
      style={{
        width: "100vw",
        height: "100vh",
        backgroundPosition: "center",
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        backgroundImage: `linear-gradient(rgba(0, 0, 0, 0.60), rgba(0, 0, 0, 0.20)), url("../glassesImage/background.jpg")`,
      }}
    >
      <Head />
      <Body />
    </div>
  );
}

export default App;
